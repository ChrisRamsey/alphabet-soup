let com = {};

com.model = {
    raw_input: "",
    max_rows: 0,
    max_cols: 0,
    matrix: [],
    letter_keys: {},
    terms: [],
    terms_with_spaces: [],
    words_found: [],
    output: "",

    reset_data: function() {
        this.raw_input = "";
        this.max_rows = 0;
        this.max_cols = 0;
        this.matrix = [];
        this.letter_keys = {};
        this.terms = [];
        this.terms_with_spaces = [];
        this.words_found = [];
        this.output = "";
    }
};

com.view = {
    display_raw_input: function() {
        let M = com.model;
        console.log( M.raw_input );
    },

    generate_puzzle_cell: function( letter ) {
        let html = "<div class='puzzle-cell'>";
        html += letter;
        return html + "</div>"
    },

    generate_puzzle_board: function( matrix ) {
        let html = "<div class='puzzle-board'>";

        for ( var i = 0; i < matrix.length; ++i ) {
            html += "<div class='puzzle-row'>";
            for (var j = 0; j < matrix[i].length; ++j ) {
                html += this.generate_puzzle_cell( matrix[i][j] );
            }
            html += "</div>";
        }

        return html + "</div>";
    },

    display_puzzle: function() {
        let M = com.model;
        document.getElementById("puzzle").innerHTML = this.generate_puzzle_board( M.matrix );
    },

    display_solution: function() {
        let M = com.model;
        console.log( M.output );
        document.getElementById("solution").innerHTML = M.output.split("\n").join("<br />");
    },

    display_input_file_name: function( file_name ) {
        const error_message_element = document.getElementById("input-file-selected-file-name");
        error_message_element.innerHTML = file_name ;
        error_message_element.classList.remove("display-none");
    },

    reset_input_file_name: function() {
        const error_message_element = document.getElementById("input-file-selected-file-name");
        error_message_element.innerHTML = "" ;
        error_message_element.classList.add("display-none");
    },

    display_input_error_message: function( error_message ) {
        const error_message_element = document.getElementById("input-file-error-message");
        error_message_element.innerHTML = error_message ;
        error_message_element.classList.remove("display-none");
    },

    reset_input_error_message: function() {
        const error_message_element = document.getElementById("input-file-error-message");
        error_message_element.innerHTML = "";
        error_message_element.classList.add("display-none");
    },

    display_results: function() {
        this.display_raw_input();
        this.display_puzzle();
        this.display_solution();

        document.getElementById("results").classList.remove("display-none");
    },

    reset_results: function() {
        document.getElementById("results").classList.add("display-none");
        document.getElementById("puzzle").innerHTML = "";
        document.getElementById("solution").innerHTML = "";
    }
};

com.controller = {
    /*
        Main listener for opening file locator to select file for processing.
    */
    activate_input_listeners: function() {
        let fileHandle;
        const input_element = document.getElementById("input-file");
        input_element.addEventListener("click", async function() {
            const M = com.model;
            const C = com.controller;
            M.reset_data();

            [fileHandle] = await window.showOpenFilePicker();
            const file = await fileHandle.getFile();
            C.read_contents_of_selected_file( file ); 
        });
    },

    /*
        Asynchronous function. Light error checking for selected file.
    */
    read_contents_of_selected_file: async function( file ) {
        let file_name = file.name;
        let V = com.view;
        V.display_input_file_name( file_name );
        V.reset_input_error_message();

        //error conditions
        //is a txt file
        if ( file_name.indexOf(".txt") != file_name.length - 4 ) {
            V.reset_results();
            V.display_input_error_message("This file is not a .txt file.");
            return;
        }

        const contents = await file.text();
        //test txt contents
        try {
            this.execute_data_processing( contents );
        } catch (error) {
            V.reset_results();
            V.display_input_error_message("This file did not process properly.");
            return;
        }
    },

    /*
        Parses content of selected file into model data then finds and displays solution
    */
    execute_data_processing: function( contents ) {
        let M = com.model;
        M.raw_input = contents;

        this.format_data();
        this.generate_letter_key();
        this.produce_word_search_key();

        let V = com.view;
        V.reset_results();
        V.display_results();
    },

    format_data: function() {
        let M = com.model;
        let raw_input = M.raw_input;

        let input_array = raw_input.split("\n");
        let dimensions_array = input_array.shift().split("x");
        M.max_rows = parseInt(dimensions_array[0]);
        M.max_cols = parseInt(dimensions_array[1]);

        let matrix = [];
        for ( var i = 0; i < M.max_rows; ++i ) {
            matrix.push( input_array.shift().split(" ") );
        }
        M.matrix = matrix;

        let terms = [];
        let terms_with_spaces = [];
        for ( var i = 0; i < input_array.length; ++i ) {
            if ( input_array[i].length > 0 ) {
                terms.push( input_array[i].split(" ").join("") );
                terms_with_spaces.push( input_array[i] );
            }
        }
        M.terms = terms;
        M.terms_with_spaces = terms_with_spaces;
    },

    generate_letter_key: function() {
        let M = com.model;
        let letter_keys = M.letter_keys;
        let matrix = M.matrix;

        for ( var i = 0; i < matrix.length; ++i ) {
            for ( var j = 0; j < matrix[i].length; ++j ) {
                var letter = matrix[i][j];
                if ( !letter_keys.hasOwnProperty(letter) ) {
                    letter_keys[letter] = [];
                }
                letter_keys[letter].push([i, j]);
            }
        }
    },

    verify_next_letter: function( matrix, row, column, letter ) {
        if ( matrix[row][column] === letter) {
            return true;
        } 
        return false;
    },

    add_solution_to_words_found: function( term_with_spaces, row, column, solution_row, solution_column, words_found) {
        let solution_array = [term_with_spaces, row + ":" + column, solution_row + ":" + solution_column]
        words_found.push( solution_array );
        com.model.output += solution_array.join(" ") + "\n";
    },

    search_up: function( matrix, row, column, term, words_found, term_with_spaces ) {
        let d = 0;
        for ( var i = row - (term.length - 1); i < row + 1; ++i ) {
            var verified = this.verify_next_letter( matrix, i, column, term[term.length - d - 1]);
            if (verified) {
                d++; 
                if (d === term.length) {
                    let solution_row = row - (term.length - 1);
                    this.add_solution_to_words_found( term_with_spaces, row, column, solution_row, column, words_found );
                    return true;
                }
            } else {
                return false;
            }
        }
    },

    search_down: function( matrix, row, column, term, words_found, term_with_spaces ) {
        let d = 0;
        for ( var i = row + term.length - 1; i >= row; --i ) {
            var verified = this.verify_next_letter( matrix, i, column, term[term.length - d - 1]);
            if (verified) {
                d++; 
                if (d === term.length) {
                    let solution_row = row + (term.length - 1);
                    this.add_solution_to_words_found( term_with_spaces, row, column, solution_row, column, words_found );
                    return true;
                }
            } else {
                return false;
            }
        }
    },

    search_right: function( matrix, row, column, term, words_found, term_with_spaces ) {
        let d = 0;
        for ( var i = column + term.length - 1; i >= column; --i ) {
            var verified = this.verify_next_letter( matrix, row, i, term[term.length - d - 1]);
            if (verified) {
                d++; 
                if (d === term.length) {
                    let solution_col = column + (term.length - 1);
                    this.add_solution_to_words_found( term_with_spaces, row, column, row, solution_col, words_found );
                    return true;
                }
            } else {
                return false;
            }
        }
    },

    search_left: function( matrix, row, column, term, words_found, term_with_spaces ) {
        let d = 0;
        for ( var i = column - (term.length - 1); i < column + 1; ++i ) {
            var verified = this.verify_next_letter( matrix, row, i, term[term.length - d - 1]);
            if (verified) {
                d++; 
                if (d === term.length) {
                    let solution_col = column - (term.length - 1);
                    this.add_solution_to_words_found( term_with_spaces, row, column, row, solution_col, words_found );
                    return true;
                }
            } else {
                return false;
            }
        }
    },

    search_right_down: function( matrix, row, column, term, words_found, term_with_spaces ) {
        let d = 0;
        for ( var i = column + term.length - 1; i >= column; --i ) {
            var verified = this.verify_next_letter( matrix, row + (term.length - d - 1) , i, term[term.length - d - 1]);
            if (verified) {
                d++; 
                if (d === term.length) {
                    let solution_row = row + (term.length - 1);
                    let solution_col = column + (term.length - 1);
                    this.add_solution_to_words_found( term_with_spaces, row, column, solution_row, solution_col, words_found );
                    return true;
                }
            } else {
                return false;
            }
        }
    },

    search_left_down: function( matrix, row, column, term, words_found, term_with_spaces ) {
        let d = 0;
        for ( var i = column - (term.length - 1); i < column + 1; ++i ) {
            var verified = this.verify_next_letter( matrix, row + (term.length - d - 1) , i, term[term.length - d - 1]);
            if (verified) {
                d++; 
                if (d === term.length) {
                    let solution_row = row + (term.length - 1);
                    let solution_col = column - (term.length - 1);
                    this.add_solution_to_words_found( term_with_spaces, row, column, solution_row, solution_col, words_found );
                    return true;
                }
            } else {
                return false;
            }
        }
    },

    search_left_up: function( matrix, row, column, term, words_found, term_with_spaces ) {
        let d = 0;
        for ( var i = column - (term.length - 1); i < column + 1; ++i ) {
            var verified = this.verify_next_letter( matrix, row - (term.length - d - 1) , i, term[term.length - d - 1]);
            if (verified) {
                d++; 
                if (d === term.length) {
                    let solution_row = row - (term.length - 1);
                    let solution_col = column - (term.length - 1);
                    this.add_solution_to_words_found( term_with_spaces, row, column, solution_row, solution_col, words_found );
                    return true;
                }
            } else {
                return false;
            }
        }
    },

    search_right_up: function( matrix, row, column, term, words_found, term_with_spaces ) {
        let d = 0;
        for ( var i = column + term.length - 1; i >= column; --i ) {
            var verified = this.verify_next_letter( matrix, row - (term.length - d - 1) , i, term[term.length - d - 1]);
            if (verified) {
                d++; 
                if (d === term.length) {
                    let solution_row = row - (term.length - 1);
                    let solution_col = column + (term.length - 1);
                    this.add_solution_to_words_found( term_with_spaces, row, column, solution_row, solution_col, words_found );
                    return true;
                }
            } else {
                return false;
            }
        }
    },

    /*
        Main function that produces the answer key solution
        Letter key stores [y, x] position of each instance of each letter available in the puzzle.
        Searches for the word calculate whether the word can be in the puzzle from a given direction.
        Skips if it cannot. 
    */
    produce_word_search_key: function(){
        let M = com.model;
        let max_rows = M.max_rows;
        let max_cols = M.max_cols;
        let matrix = M.matrix;
        let letter_keys = M.letter_keys;
        let terms = M.terms;
        let terms_with_spaces = M.terms_with_spaces;
        let words_found = M.words_found;

        for (var i = 0; i < terms.length; ++i) {
            let word_was_found = false;
            let term_length = terms[i].length;
            for ( var j = 0; j < letter_keys[terms[i][0]].length; ++j) {
                let start_row = letter_keys[terms[i][0]][j][0];
                let start_col = letter_keys[terms[i][0]][j][1];

                if (word_was_found) break;

                // 8 directions, but don't search if the word would be too long in that direction.
                // can the word be directly upward?
                if ( start_row >= term_length - 1 && 
                    !word_was_found ) {
                    word_was_found = this.search_up( matrix, start_row, start_col, terms[i], words_found, terms_with_spaces[i] );
                }

                // can the word be directly downward?
                if ( start_row <= max_rows - term_length && 
                    !word_was_found ) {
                    word_was_found = this.search_down( matrix, start_row, start_col, terms[i], words_found, terms_with_spaces[i] );
                }

                // can the word be directly to the right?
                if ( start_col <= max_cols - term_length && 
                    !word_was_found ) {
                    word_was_found = this.search_right( matrix, start_row, start_col, terms[i], words_found, terms_with_spaces[i] );
                }
                // can the word be directly to the left?
                if ( start_col >= term_length - 1 && 
                     !word_was_found ) {
                    word_was_found = this.search_left( matrix, start_row, start_col, terms[i], words_found, terms_with_spaces[i] );
                }

                // can the word be directly down to the right?
                if ( (start_row <= max_rows - term_length) && 
                     (start_col <= max_cols - term_length) && 
                    !word_was_found ) {
                    word_was_found = this.search_right_down( matrix, start_row, start_col, terms[i], words_found, terms_with_spaces[i] );
                }

                // can the word be directly down to the left?
                if ( (start_row <= max_rows - term_length) && 
                     (start_col >= term_length - 1) && 
                     !word_was_found ) {
                    word_was_found = this.search_left_down( matrix, start_row, start_col, terms[i], words_found, terms_with_spaces[i] );
                }

                // can the word be directly up to the left?
                if ( (start_row >= term_length - 1) && 
                     (start_col >= term_length - 1) && 
                     !word_was_found ) {
                    word_was_found = this.search_left_up( matrix, start_row, start_col, terms[i], words_found, terms_with_spaces[i] );
                }
           
                // can the word be directly up to the right?
                if ( (start_row >= term_length - 1) && 
                     (start_col <= max_cols - term_length) && 
                     !word_was_found ) {
                    word_was_found = this.search_right_up( matrix, start_row, start_col, terms[i], words_found, terms_with_spaces[i] );
                }
            }
        }
    },

    init: function() {
        this.activate_input_listeners();
    }
};

document.addEventListener('DOMContentLoaded', function () {
    com.controller.init();
}, false);